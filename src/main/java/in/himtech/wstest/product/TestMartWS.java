package in.himtech.wstest.product;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService(serviceName = "TestMartService", portName = "TestMartServicePort", 
targetNamespace = "http://wstest.himtech.in")
public class TestMartWS {
	private List<String> listBooks;
	private List<String> listPictures;
	private List<String> listMusic;
	private List<String> listMovies;

	public TestMartWS() {
		listBooks = new ArrayList<>();
		listBooks.add("Soap Web Service");
		listBooks.add("Restful Web Service");
		listBooks.add("Hibernate");
		listBooks.add("Spring MVC");

		listPictures = new ArrayList<>();
		listPictures.add("Himanshu");
		listPictures.add("Anushka");
		listPictures.add("Hritika");
		listPictures.add("Hrishik");

		listMusic = new ArrayList<>();
		listMusic.add("Lata");
		listMusic.add("Rafi");
		listMusic.add("Kishore");
		listMusic.add("Mukesj");

		listMovies = new ArrayList<>();
		listMovies.add("Tevar");
		listMovies.add("Baby");
		listMovies.add("Hawaijade");
		listMovies.add("Alone");
	}

	@WebMethod(action = "product_by_category", operationName = "productByCategory")
	@WebResult(name = "listOfProduct", partName = "list_product")
	public List<String> getProductList(String category)
			throws IllegalInputException {
		List<String> list = new ArrayList<>();

		if (category.equalsIgnoreCase("Books")) {
			list = listBooks;
		} else if (category.equals("Musics")) {
			list = listMusic;
		} else if (category.equals("Pictures")) {
			list = listPictures;
		} else if (category.equals("Moives")) {
			list = listMovies;
		} else {
			throw new IllegalInputException(category
					+ " is not a valid category.");
		}

		return list;
	}

	@WebMethod(action = "add_product", operationName = "addProduct")
	@WebResult(name = "isProductAdded", partName = "is_product_added")
	public boolean addProduct(String category, String product) {
		boolean isAdded = false;
		if (category.equalsIgnoreCase("Books")) {
			isAdded = listBooks.add(product);
		} else if (category.equals("Musics")) {
			isAdded = listMusic.add(product);
		} else if (category.equals("Pictures")) {
			isAdded = listPictures.add(product);
		} else if (category.equals("Moives")) {
			isAdded = listMovies.add(product);
		} else {
			isAdded = false;
		}
		return isAdded;
	}

	@WebMethod(action = "list_productCategory", operationName = "listProductCategory")
	@WebResult(name="listOfProdCatgory", partName="list_pordCategory")
	public List<String> getProductCategoryList() {
		List<String> listProduct = new ArrayList<String>();

		listProduct.add("Books");
		listProduct.add("Pictures");
		listProduct.add("Music");
		listProduct.add("Movies");

		return listProduct;
	}
}
