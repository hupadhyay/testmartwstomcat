package in.himtech.wstest.product;

public class IllegalInputException extends Exception {
	private String message;
	
	public IllegalInputException() {
		// TODO Auto-generated constructor stub
	}
	
	public IllegalInputException(String msg){
		super(msg);
		message=msg;
	}
	
	public IllegalInputException(String msg, Throwable initCause){
		super(msg, initCause);
		message=msg;
	}
	
	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return super.getMessage();
	}
}
